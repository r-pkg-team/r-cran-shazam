Source: r-cran-shazam
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Steffen Moeller <moeller@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-ggplot2 (>= 3.4.0),
               r-cran-alakazam (>= 1.3.0),
               r-cran-ape,
               r-cran-diptest,
               r-cran-doparallel,
               r-cran-dplyr,
               r-cran-foreach,
               r-cran-igraph (>= 1.5.0),
               r-cran-iterators,
               r-cran-kernsmooth,
               r-cran-lazyeval,
               r-cran-mass,
               r-cran-progress,
               r-cran-rlang,
               r-cran-scales,
               r-cran-seqinr,
               r-cran-stringi,
               r-cran-tidyr,
               r-cran-tidyselect
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-shazam
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-shazam.git
Homepage: https://cran.r-project.org/package=shazam
Rules-Requires-Root: no

Package: r-cran-shazam
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: Immunoglobulin Somatic Hypermutation Analysis
 Provides a computational framework for Bayesian estimation of
 antigen-driven selection in immunoglobulin (Ig) sequences, providing an
 intuitive means of analyzing selection by quantifying the degree of
 selective pressure. Also provides tools to profile mutations in Ig
 sequences, build models of somatic hypermutation (SHM) in Ig sequences,
 and make model-dependent distance comparisons of Ig repertoires.
 .
 SHazaM is part of the Immcantation analysis framework for Adaptive
 Immune Receptor Repertoire sequencing (AIRR-seq) and provides tools for
 advanced analysis of somatic hypermutation (SHM) in immunoglobulin (Ig)
 sequences. Shazam focuses on the following analysis topics:
 .
  * Quantification of mutational load
    SHazaM includes methods for determine the rate of observed and
    expected mutations under various criteria. Mutational profiling
    criteria include rates under SHM targeting models, mutations specific
    to CDR and FWR regions, and physicochemical property dependent
    substitution rates.
  * Statistical models of SHM targeting patterns
    Models of SHM may be divided into two independent components:
     1) a mutability model that defines where mutations occur and
     2) a nucleotide substitution model that defines the resulting mutation.
    Collectively these two components define an SHM targeting
    model. SHazaM provides empirically derived SHM 5-mer context mutation
    models for both humans and mice, as well tools to build SHM targeting
    models from data.
  * Analysis of selection pressure using BASELINe
    The Bayesian Estimation of Antigen-driven Selection in Ig Sequences
    (BASELINe) method is a novel method for quantifying antigen-driven
    selection in high-throughput Ig sequence data. BASELINe uses SHM
    targeting models can be used to estimate the null distribution of
    expected mutation frequencies, and provide measures of selection
    pressure informed by known AID targeting biases.
  * Model-dependent distance calculations
    SHazaM provides methods to compute evolutionary distances between
    sequences or set of sequences based on SHM targeting models. This
    information is particularly useful in understanding and defining
    clonal relationships.
